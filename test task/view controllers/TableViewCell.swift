//
//  TableViewCell.swift
//  test task
//
//  Created by Galina Fedorova on 07/05/2018.
//  Copyright © 2018 Galina Fedorova. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var suitesAvailabilityLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        photo.frame.size.height = 94
//        photo.frame.size.width = 146
//        photo.frame.origin = CGPoint(x: 16, y: 5)
//        nameLabel.frame = CGRect(x: 239, y: 8, width: 70, height: 20)
//        addressLabel.frame = CGRect(x: 251, y: 33, width: 70, height: 20)
//        starsLabel.frame = CGRect(x: 233, y: 58, width: 70, height: 20)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
