//
//  TableViewController.swift
//  test task
//
//  Created by Galina Fedorova on 07/05/2018.
//  Copyright © 2018 Galina Fedorova. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    private var requestManager = RequestManager()
    private var arrayOfHotel:[HotelModel]?
    private var activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
            super.viewDidLoad()
        
        if let navigationBarWidth  = self.navigationController?.navigationBar.frame.width{
           self.segmentControl.frame.size.width = navigationBarWidth-40
        }
        
        segmentControl.isHidden = true
        self.view.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.view.center
        activityIndicator.color = UIColor.black
        activityIndicator.startAnimating()
        
        if !CheckInternetConnection.isConnectedToNetwork(){
            
            addAlert(title: "Internet Connection not Available!", message: "Сheck your internet connection")
            
        } else {
            
            requestManager.callbackForHotelsInfo = { (hotels, error) in
                
                if let error = error{
                    self.addAlert(title: "Error", message: error.localizedDescription)
                }
                
                guard let hotels = hotels else {return}
                self.arrayOfHotel = hotels
                
                DispatchQueue.main.async {
                    self.segmentControl.isHidden = false
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                }
                
            }
            self.requestManager.requestForHotelsInfo(urlString: "https://raw.githubusercontent.com/iMofas/ios-android-test/master/0777.json")

        }
    }
    
    func addAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {(action) in
            self.activityIndicator.stopAnimating()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sortedByParamrtes(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            arrayOfHotel?.sort(){ $0.distance < $1.distance}
        }
        if sender.selectedSegmentIndex == 1{
            arrayOfHotel?.sort(by: { (first, two) -> Bool in
                if let firstItem = first.suites_availability, let secondItem = two.suites_availability{
                    return firstItem.count > secondItem.count
                }else{
                    return false
                }
            })
        }
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = arrayOfHotel?.count{
            return count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        
        if let arrayOfHotel = arrayOfHotel{
            cell.nameLabel.text = arrayOfHotel[indexPath.row].name
            cell.addressLabel.text = arrayOfHotel[indexPath.row].address
            cell.starsLabel.text = String(arrayOfHotel[indexPath.row].stars)
            cell.distanceLabel.text = String(arrayOfHotel[indexPath.row].distance)
            cell.suitesAvailabilityLabel.text = arrayOfHotel[indexPath.row].suites_availabilityString.replacingOccurrences(of: ":", with: ",")
        }
    
        return cell
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController{
            
            if let id = arrayOfHotel?[indexPath.row].id{
                vc.id = String(id)
            }
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
 
}
